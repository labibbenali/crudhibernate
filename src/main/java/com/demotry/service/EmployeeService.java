package com.demotry.service;
import com.demotry.model.Employee;
import org.springframework.stereotype.Service;

import java.util.List;

public interface EmployeeService {

    public List<Employee> findAll();
    public Employee findEmployeeById(int employeeId) ;
    public void save(Employee employee);
    public  void deleteById(int theId);

}
