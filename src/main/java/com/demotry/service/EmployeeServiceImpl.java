package com.demotry.service;

import com.demotry.dao.EmployeeDao;
import com.demotry.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeDao employeeDao;
    @Autowired
    public EmployeeServiceImpl(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    @Override
    @Transactional
    public List<Employee> findAll() {
        return this.employeeDao.findAll();
    }

    @Override
    @Transactional
    public Employee findEmployeeById(int employeeId) {
        return this.employeeDao.findEmployeeById(employeeId);
    }

    @Override
    @Transactional
    public void save(Employee employee) {
        this.employeeDao.save(employee);
    }

    @Override
    @Transactional
    public void deleteById(int theId) {
        this.employeeDao.deleteById(theId);

    }
}
