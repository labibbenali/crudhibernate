package com.demotry.controller;

import com.demotry.model.Employee;
import com.demotry.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeRestController {

    private final EmployeeService employeeService;
    @Autowired
    public EmployeeRestController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }
    //get all emplyees
    @GetMapping("/employees")
    public List<Employee> getEmplyees(){
        return this.employeeService.findAll();
    }
    //get employee by id
    @GetMapping("/employees/{id}")
    public Employee getEmployeeById(@PathVariable int id){
    Employee theEmployee=employeeService.findEmployeeById(id);
    if(theEmployee==null){
        throw new RuntimeException("Employee id not exist - "+id);
    }
    return  theEmployee;
    }

    @PostMapping("/employees")
    public Employee createEmployee( @RequestBody Employee employee){
        employee.setId(0);
          this.employeeService.save(employee);
          return  employee;
    }

    @PutMapping("/employees")
    public Employee updateEmployee(@RequestBody Employee employee){
        System.out.println("this first emplyee  :  "+employee);
        employeeService.save(employee);
        System.out.println("this SECONDE emplyee  :  "+employee);
        return employee;
    }

    @DeleteMapping("/employees/{id}")
    public String deleteEmployee(@PathVariable int id){
        Employee employee=this.employeeService.findEmployeeById(id);
        if(employee==null){
            throw new RuntimeException("Employee id not found"+id);
        }
        employeeService.deleteById(id);
        return "Deleted employee id - "+ id;
    }

}
