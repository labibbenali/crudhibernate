package com.demotry.dao;

import com.demotry.model.Employee;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class EmployeeDAOHibernateImpl implements EmployeeDao{
    //define field for entityManager
    private EntityManager entityManager;
    //set up constructor injection
    @Autowired
    public EmployeeDAOHibernateImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public List<Employee> findAll(){
        //get the current hibernate session
        Session currentSession=entityManager.unwrap(Session.class);
        //create a query
        Query<Employee> theQuery= currentSession.createQuery("from Employee",Employee.class);
        // execute query and get result list
        List<Employee> employees=theQuery.getResultList();
        //return the results
        return  employees;
    }
    @Override
    public Employee findEmployeeById(int theId){
    Session currentSession=entityManager.unwrap(Session.class);
    Employee employee=currentSession.get(Employee.class, theId);
        return  employee;
    }
    @Override
    public void save(Employee employee) {
    Session currentSession = entityManager.unwrap(Session.class);
    currentSession.saveOrUpdate(employee);
    }
    @Override
    public void deleteById(int theId) {
    Session  currentSession=entityManager.unwrap(Session.class);
    Query theQuery=currentSession.createQuery("delete from Employee where id=:employeeId");
    theQuery.setParameter("employeeId",theId);
    theQuery.executeUpdate();
    }
}
