package com.demotry.dao;

import com.demotry.model.Employee;

import java.util.List;

public interface EmployeeDao {
    public List<Employee> findAll();
    public Employee findEmployeeById(int employeeId) ;
    public void save(Employee employee);
    public  void deleteById(int theId);
}
